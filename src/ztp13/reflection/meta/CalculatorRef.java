package ztp13.reflection.meta;

import java.awt.GridLayout;
import java.util.Iterator;
import javax.swing.*;
import ztp13.reflection.Acumulator;

/**
 *
 * @author Damian Terlecki
 */
public class CalculatorRef extends JFrame {

    public static CalculatorRef calculator = new CalculatorRef();
    JTextField screen = new JTextField("0");
    Acumulator acumulator = new Acumulator();
    private final JPanel mainPanel = new JPanel();

    private CalculatorRef() {
        super("Reflection calculator DEMO");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        add(mainPanel);
    }

    public void build() {
        mainPanel.removeAll();
        Iterator<JComponent> componentsIterator = MetaInfo.metaInfo.iterator();
        mainPanel.setLayout(new GridLayout(10, 1));
        mainPanel.add(screen);
        int counter = 0;
        JPanel panel = null;
        ButtonGroup radiosGroup = new ButtonGroup();
        while (componentsIterator.hasNext()) {
            if (counter == 0) {
                panel = new JPanel();
                panel.setLayout(new GridLayout(1, 4));
                mainPanel.add(panel);
            }
            JComponent component = componentsIterator.next();
            if (component instanceof JRadioButton) {
                radiosGroup.add((AbstractButton) component);
                ((JRadioButton) component).doClick();
            }
            panel.add(component);
            counter = (counter + 1) % 4;
        }
        pack();
    }

}
