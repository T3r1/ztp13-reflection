package ztp13.reflection.meta;

import java.lang.reflect.Method;
import java.util.*;
import javax.swing.*;
import ztp13.reflection.*;

/**
 *
 * @author Damian Terlecki
 */
public class MetaInfo {

    public static MetaInfo metaInfo = new MetaInfo();
    private final List<JComponent> buttons = new LinkedList<>();

    private MetaInfo() {
    }

    public Iterator<JComponent> iterator() {
        return buttons.iterator();
    }

    public void addMethods(Class c) throws InstantiationException, IllegalAccessException {
        Acumulator acumulator = CalculatorRef.calculator.acumulator;
        JTextField screen = CalculatorRef.calculator.screen;
        Object object = c.newInstance();
        for (Method method : c.getMethods()) {
            Class[] types = method.getParameterTypes();
            if (types.length != 2 || !types[0].equals(Acumulator.class) || !types[1].equals(JTextField.class)) {
                if (types.length != 1 || !types[0].equals(Acumulator.class)) {
                    continue;
                } else {
                    Name name = method.getAnnotation(Name.class);
                    String radioName;
                    if (name != null) {
                        radioName = name.value();
                    } else {
                        radioName = method.getName();
                    }
                    buttons.add(new RadioButton(radioName, method, object, acumulator, screen));
                    continue;
                }
            }
            Name name = method.getAnnotation(Name.class);
            String buttonName;
            if (name != null) {
                buttonName = name.value();
            } else {
                buttonName = method.getName();
            }
            buttons.add(new Button(buttonName, method, object, acumulator, screen));
        }
    }

}
