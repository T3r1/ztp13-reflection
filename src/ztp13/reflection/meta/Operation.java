package ztp13.reflection.meta;

import java.math.BigDecimal;
import javax.swing.JTextField;
import ztp13.reflection.Acumulator;

/**
 *
 * @author Damian Terlecki
 */
public class Operation {

    @Name("+")
    public void plus(Acumulator acumulator, JTextField screen) {
        BigDecimal number1 = acumulator.getState();
        BigDecimal number2 = new BigDecimal(screen.getText().trim());
        screen.setText(number1.add(number2).toPlainString());
    }

    @Name("-")
    public void minus(Acumulator acumulator, JTextField screen) {
        BigDecimal number1 = acumulator.getState();
        BigDecimal number2 = new BigDecimal(screen.getText().trim());
        screen.setText(number1.subtract(number2).toPlainString());
    }

    @Name("CLEAR")
    public void clearScreen(Acumulator acumulator, JTextField screen) {
        screen.setText("0");
    }
    
    @Name("CLEAR AKU")
    public void clearAcumulator(Acumulator acumulator, JTextField screen) {
        acumulator.setState(BigDecimal.ZERO);
    }
}
