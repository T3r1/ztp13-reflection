package ztp13.reflection.meta;

import javax.swing.JTextField;
import ztp13.reflection.Acumulator;

/**
 *
 * @author Damian Terlecki
 */
public class Number {

    @Name("0")
    public void l0(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("0");
        } else {
            screen.setText(screen.getText() + "0");
        }
    }

    @Name("1")
    public void l1(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("1");
        } else {
            screen.setText(screen.getText() + "1");
        }
    }

    @Name("2")
    public void l2(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("2");
        } else {
            screen.setText(screen.getText() + "2");
        }
    }

    @Name("3")
    public void l3(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("3");
        } else {
            screen.setText(screen.getText() + "3");
        }
    }

    @Name("4")
    public void l4(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("4");
        } else {
            screen.setText(screen.getText() + "4");
        }
    }

    @Name("5")
    public void l5(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("5");
        } else {
            screen.setText(screen.getText() + "5");
        }
    }

    @Name("6")
    public void l6(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("6");
        } else {
            screen.setText(screen.getText() + "6");
        }
    }

    @Name("7")
    public void l7(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("7");
        } else {
            screen.setText(screen.getText() + "7");
        }
    }

    @Name("8")
    public void l8(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("8");
        } else {
            screen.setText(screen.getText() + "8");
        }
    }

    @Name("9")
    public void l9(Acumulator acumulator, JTextField screen) {
        if (screen.getText().equals("0")) {
            screen.setText("9");
        } else {
            screen.setText(screen.getText() + "9");
        }
    }

    @Name(".")
    public void lp(Acumulator acumulator, JTextField screen) {
        screen.setText(screen.getText() + ".");
    }
}
