package ztp13.reflection.meta;

import java.math.BigDecimal;
import javax.swing.JTextField;
import ztp13.reflection.Acumulator;

/**
 *
 * @author Damian Terlecki
 */
public class AcumulatorFunction {

    @Name("->Akum")
    public void toAcumulator(Acumulator acumulator, JTextField screen) {
        acumulator.setState(new BigDecimal(screen.getText().trim()));
        screen.setText("0");
    }

    @Name("<-Akum")
    public void fromAcumulator(Acumulator acumulator, JTextField screen) {
        screen.setText(acumulator.getState().toPlainString());
    }
}
