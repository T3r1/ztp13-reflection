package ztp13.reflection.meta;

import javax.swing.*;
import ztp13.reflection.Acumulator;

/**
 *
 * @author Damian Terlecki
 */
public class OperationsAdder {

    @Name("Więcej")
    public void more(Acumulator acumulator, JTextField screen) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        String className = (String) JOptionPane.showInputDialog(CalculatorRef.calculator,
                "Podaj nazwę klasy", "Załaduj dodatkowe opcje", JOptionPane.QUESTION_MESSAGE, null, null, "ztp13.reflection.meta.Trigonometry");
        Class c = Class.forName(className);
        MetaInfo.metaInfo.addMethods(c);
        CalculatorRef.calculator.build();
    }
}
