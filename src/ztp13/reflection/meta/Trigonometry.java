package ztp13.reflection.meta;

import java.math.BigDecimal;
import javax.swing.JTextField;
import ztp13.reflection.Acumulator;

/**
 *
 * @author Damian Terlecki
 */
public class Trigonometry {

    @Name("sin()")
    public void sin(Acumulator acumulator, JTextField screen) {
        if (acumulator.isRadians()) {
            screen.setText(Double.toString(Math.sin(new BigDecimal(screen.getText().trim()).doubleValue())));
        } else {
            screen.setText(Double.toString(Math.sin(Math.toRadians(new BigDecimal(screen.getText().trim()).doubleValue()))));
        }
    }

    @Name("cos()")
    public void cos(Acumulator acumulator, JTextField screen) {
        if (acumulator.isRadians()) {
            screen.setText(Double.toString(Math.cos(new BigDecimal(screen.getText().trim()).doubleValue())));
        } else {
            screen.setText(Double.toString(Math.cos(Math.toRadians(new BigDecimal(screen.getText().trim()).doubleValue()))));
        }
    }

    @Name("Radiany")
    public void radians(Acumulator acumulator) {
        acumulator.setRadians(true);
    }

    @Name("Stopnie")
    public void degrees(Acumulator acumulator) {
        acumulator.setRadians(false);
    }

}
