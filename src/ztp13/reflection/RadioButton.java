package ztp13.reflection;

import java.lang.reflect.*;
import java.util.logging.*;
import javax.swing.*;

/**
 *
 * @author Damian Terlecki
 */
public class RadioButton extends JRadioButton {

    private final Method method;
    private final Object object;
    private final Acumulator acumulator;
    private final JTextField screen;

    public RadioButton(String text, Method method, Object object, Acumulator acumulator, JTextField screen) {
        super(text);
        this.method = method;
        this.object = object;
        this.acumulator = acumulator;
        this.screen = screen;

        addActionListener((actionEvent) -> {
            try {
                method.invoke(object, new Object[]{acumulator});
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(Button.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }
}
