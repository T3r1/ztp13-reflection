package ztp13.reflection;

import java.math.BigDecimal;

/**
 *
 * @author Damian Terlecki
 */
public class Acumulator {

    private BigDecimal state = new BigDecimal("0");
    private boolean radians = true;

    public BigDecimal getState() {
        return state;
    }

    public void setState(BigDecimal state) {
        this.state = state;
    }

    public boolean isRadians() {
        return radians;
    }

    public void setRadians(boolean radians) {
        this.radians = radians;
    }

}
