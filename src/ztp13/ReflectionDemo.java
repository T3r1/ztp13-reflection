package ztp13;

import ztp13.reflection.meta.*;
import ztp13.reflection.meta.Number;

/**
 *
 * @author Damian Terlecki
 */
public class ReflectionDemo {

    /**
     * @param args the command line arguments
     * @throws java.lang.InstantiationException
     * @throws java.lang.IllegalAccessException
     */
    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
        MetaInfo metaInfo = MetaInfo.metaInfo;
        CalculatorRef calculator = CalculatorRef.calculator;
        metaInfo.addMethods(Number.class);
        metaInfo.addMethods(Operation.class);
        metaInfo.addMethods(AcumulatorFunction.class);
        metaInfo.addMethods(OperationsAdder.class);
        calculator.build();
        calculator.setVisible(true);
    }

}
